# Preparation
- [ ] Develop Agenda items and sequencing of topics
- [ ] Content Assignments
- [ ] Prepare Projects for demonstration
- [ ] Prepare Slides for presentation
- [ ] Dry Run (Technology walk through, Q/A responsibilities)

# Workshop Theme

### {{Workshop Title}

{{workshop Description / Copy}}

# Agenda

**Agenda Draft Approved by**
* [ ] Instructor Approval 
* [ ] Customer Success Approval
* [ ] Agenda updates are approved to add to event


| Time | Agenda Topic | Presenter Role | Assigned |
| ------ | ------ | ------ | ---- |


# Roles

### Presenters
Presenters will cover the supplied course content and lab execution for attendees. The Presenter is responsible for understanding and delivery the course subject matter to attendees in clear and conciese manner. The Prsenter is able to modify content to align with the audence, however this content should be contributed back to the base project. 

### Master of Ceremonies (MC)
The MC will be the director of the webinar. This role will be in front of the camera: introducing presenters, announcing breaks, announcing poll questions/results, closing. Idea for this role is to have a familiar voice/face for the attendees to understand who will guide them through the workshop.

### Time Keeper
* Keep the webinar on schedule. Keep speakers on track, notify what's next  (Sessions, QA, Breaks)
* Collect questions in chat room, reach out to SAs to answer
* POC for users with technical issues

### TAs
Answers questions and assists attendees with technical issues

# Assignments
| Name | Role |
| ------ | ------ |

## Location for Slide decks and project references

GDrive Folder: {{Materials}}

Base Lab Project:{{Base Lab Project}}

| Agenda Item | Slide Deck | Labs | Project Link (If any) | Status |
| ------ | ------ | ------ | ------ | ------ |

